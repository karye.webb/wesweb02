# Table of contents

* [Introduktion](README.md)
* [Utvecklingsmiljö](utvecklingsmiljoe.md)

## Databaser och CRUD <a href="#kapitel-1" id="kapitel-1"></a>

* [Vad är CRUD?](kapitel-1/crud.md)
* [C för skapa/infoga](kapitel-1/create.md)
* [R för läsa/hitta](kapitel-1/r-foer-laesa-hitta.md)
* [U för ändra/redigera](kapitel-1/u-foer-aendra-redigera.md)
* [D för radera](kapitel-1/d-foer-radera.md)

## Api:er och JSON <a href="#kapitel-2" id="kapitel-2"></a>

* [Intro till api](kapitel-2/intro-till-api.md)
* [Labb 1 - OWM api](kapitel-2/labb-1-owm-api.md)
* [Uppgift - Nasa api](kapitel-2/uppgift-nasa-api.md)
* [Labb 2 - itunes](kapitel-2/labb-2-itunes.md)
* [Labb 3 - api.sl.se](kapitel-2/labb-3-api.sl.se.md)
* [Labb 4 - api.smhi.se](kapitel-2/labb-4-api.smhi.se.md)
* [Labb 5 - OMDB api](kapitel-2/labb-5-omdb-api.md)

## OOP PHP <a href="#kapitel-3" id="kapitel-3"></a>

* [Intro till OOP](kapitel-3/intro-till-oop.md)
* [Validator klass 1:2](kapitel-3/validator-klass-1-2.md)
* [Validator klass 2:2](kapitel-3/anvaenda-en-klass.md)
* [Login klass](kapitel-3/en-login-klass.md)
