---
description: Hämta närmaste hållplatser och visa på en karta
---

# Labb 3 - api.sl.se

## Del 1 - geolocation i webbläsaren

* I webbläsaren kan man ta reda på var användaren finns&#x20;

{% tabs %}
{% tab title="min-position.html" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Geolocation</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Geolocation</h1>
        <p></p>
    </div>
    <script>
        // Hitta element p för utskrift
        const p = document.querySelector('p');

        // Fråga användaren om man hitta dennes position
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showLocation);
        } else {
            alert("Kan inte hitta din position. Du har en gammal webbläsare.");
        }

        // Skriv ut koordinaterna 
        function showLocation(position) {
            var latHem = position.coords.latitude;
            var lonHem = position.coords.longitude;

            p.textContent = "Din position är: latitude = " + latHem + ",  longitude = " + lonHem;
        }
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
#map {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
}
.hem {
    background: url("./bilder/icons8-home-address-48.png");
    background-size: cover;
    width: 48px;
    height: 48px;
    cursor: pointer;
}
.buss {
    background: url("./bilder/icons8-bus-48.png");
    background-size: cover;
    width: 24px;
    height: 24px;
    cursor: pointer;
}
.skola {
    background: url("./bilder/icons8-school-building-48.png");
    background-size: cover;
    width: 48px;
    height: 48px;
    cursor: pointer;
}
```
{% endtab %}
{% endtabs %}

## Del 2 - visa position på karta

### Rita ut en karta

* Med biblioteket [mapbox ](https://www.mapbox.com)kan man rita upp en karta

```php
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Geolocation</title>
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet">
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <h1>Geolocation</h1>
    <div id="map"></div>
    <script>

        // En start position
        var lat = 59.336885;
        var lon = 18.048323;

        // Rita ut kartan
        mapboxgl.accessToken = 'pk.eyJ1Ijoia2FyeWUiLCJhIjoiY2pwOXRtbWc1MGdmNjNwc2JmdGxzeDR5byJ9.whp8f2Ttws57ctAf_stuag';
        var map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
            center: [lon, lat], // starting position [lng, lat]
            zoom: 12 // starting zoom
        });
    </script>
</body>
</html>
```

### Infoga en markör för position

```php
// Hitta positionen
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showLocation);
} else {
    alert("Kan inte hitta din position. Du har en gammal webbläsare.");
}

// Plocka ut koordinaterna
function showLocation(position) {
    var latHem = position.coords.latitude;
    var lonHem = position.coords.longitude;

    console.log("Din position är: longitude = " + lonHem + ", latitude = " + latHem);
    visaPåKarta(lonHem, latHem);
}

// Rita ut en markör på kartan
function visaPåKarta(lon, lat) {

    // Skapa en special markerikon för hem
    var hem = document.createElement('div');
    hem.className = 'hem';
    new mapboxgl.Marker(hem)
        .setLngLat([lon, lat])
        .addTo(map);

    // Flyg till platsen
    map.flyTo({
        center: [lon, lat],
        zoom: 15,
        speed: 1,
        curve: 1,
        easing(t) {
            return t;
        }
    });
}
```

## Del 3 - anropa SL: api

### Prova api

* Först ett formulär för att kunna mata in koordinater

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Närmaste hållplatser</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Närmaste hållplatser</h1>
        <form class="kol2" action="#" method="post">
            <label>Ange latitude</label><input type="text" name="lat">
            <label>Ange longitude</label><input type="text" name="lon">
            <button>Sök</button>
        </form>
        <?php
        // Ta emot data från formuläret
        $lat = filter_input(INPUT_POST, 'lat');
        $lon = filter_input(INPUT_POST, 'lon');

        // Om data inte är tomt gör något
        if ($lat && $lon) {


        } else {
            echo "<p>Något blev fel!</p>";
        }
        ?>
    </div>
</body>
</html>
```

### SL:s api

* Skicka anrop till api.sl.se

```php
// Api-nyckeln
$api = "5a04359da47042b7837f88a5c61908c9";

// Radie inom vilken vi vill hitta hållplatser
$radius = 500;

// Max antal hållplatser
$max = 99;

// url:en till api-tjänsten
$url = "http://api.sl.se/api2/nearbystops.json?key=$api&originCoordLat=$lat&originCoordLong=$lon&maxresults=$max&radius=$radius";

// Hämta json-data från api
$json = file_get_contents($url);
```

* .. avkoda och plocka ut lista på hållplatser

```php
// Avkodar json-datan
$jsonData = json_decode($json);

// Leta rätt på data vi är intresserade av
$stopLocation = $jsonData->LocationList->StopLocation;

// Loopa igenom alla hållplatser en-och-en
echo "<table>";
foreach ($stopLocation as $stop) {
    $name = $stop->name;
    $lat = $stop->lat;
    $lon = $stop->lon;
    echo "<tr><td>$name</td><td>$lat</td><td>$lon</td></tr>";
}
echo "</table>";
```

## Del 4 - alla delar tillsammans

### Först en karta

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hållplatser nära mig</title>
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="map"></div>
    <script defer src="hallplatser.js"></script>
</body>
</html>
```

### Frontend-koden

* Här inhämtas användarens position
* .. backend anropas
* .. och hållplatserna ritas ut

```php
// Startposition
var lat = 59.336885;
var lon = 18.048323;

mapboxgl.accessToken = 'pk.eyJ1Ijoia2FyeWUiLCJhIjoiY2pwOXRtbWc1MGdmNjNwc2JmdGxzeDR5byJ9.whp8f2Ttws57ctAf_stuag';
var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
    center: [lon, latNTI], // starting position [lng, lat]
    zoom: 12 // starting zoom
});

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showLocation);
} else {
    alert("Kan inte hitta din position. Du har en gammal webbläsare.");
}

function showLocation(position) {
    var latHem = position.coords.latitude;
    var lonHem = position.coords.longitude;
    console.log("Din position är: " + latHem + ", " + lonHem);

    // Skapa en special markerikon för hem
    var hem = document.createElement('div');
    hem.className = 'hem';
    new mapboxgl.Marker(hem)
        .setLngLat([lonHem, latHem])
        .addTo(map);

    // Flyg in till platsen
    map.flyTo({
        center: [lonHem, latHem],
        zoom: 15,
        speed: 1,
        curve: 1,
        easing(t) {
            return t;
        }
    });

    // Omvandla data till post-data
    var postdata = new FormData();
    postdata.append("lat", latHem);
    postdata.append("lon", lonHem);

    // Skicka data till ett PHP-skript
    fetch("./trafiklab.php", {
        method: 'POST',
        body: postdata
    })
    .then(response => response.json())
    .then(platser => {
        console.log(platser);

        // Loppa igenom alla hållplatser
        platser.forEach(function(plats) {
            console.log("Hållplats: ", plats);

            // Skapa en special markerikon för busshållplats
            var buss = document.createElement('div');
            buss.className = 'buss';

            // Skapa en popup med gatuadressen
            var popup = new mapboxgl.Popup({
                    offset: 25
                })
                .setText(plats.adress);

            // Infoga en marker på kartan för varje hållpats
            new mapboxgl.Marker(buss)
                .setLngLat([plats.lon, plats.lat])
                .setPopup(popup)
                .addTo(map);
        });
    })
    .catch(error => {
        console.log("Något gick fel: ", error);
    })
}
```

### Backend-koden

* PHP-koden tar emot koordinaten
* .. anropar SL:s api
* .. och returnerar JSON med hållplatserna

```php
<?php
$SVAR = 100;
$RADIUS = 1000;

$lat = filter_input(INPUT_POST, 'lat');
$lon = filter_input(INPUT_POST, 'lon');

if ($lat && $lng) {

    // Adress till tjänsten
    $url = "http://api.sl.se/api2/nearbystops.json?key=5a04359da47042b7837f88a5c61908c9&originCoordLat=$lat&originCoordLong=$lng&maxresults=$SVAR&radius=$RADIUS";

    // Hämtar data och avkodar json
    $contents = file_get_contents($url);
    $stopData = json_decode($contents);
    $stops = $stopData->LocationList->StopLocation;

    $markers = "[";

    // Plocka ut alla hållplatser
    foreach ($stops as $stop) {
        $markers .= "['$stop->name', $stop->lat, $stop->lon],\n";
    }

    // Ta bort sista kommat
    $markers = substr($markers, 0, -2) . "]";

    // Skapa Google Maps kartan som Javascript
    echo "function initMap() {
        var bounds = new google.maps.LatLngBounds();
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14
        });\n";

    echo "var markers = $markers;\n";

    echo "for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0]
            });
        }\n
        map.fitBounds(bounds);\n
    }\n
    initMap();";
}
```
