# Intro till API

## Vad är ett API?

Ett API (Application Programming Interface) är ett gränssnitt som låter olika programvaror kommunicera med varandra. API:er används ofta för att hämta data från tjänster på Internet, vilket möjliggör dynamisk datauppdatering i applikationer utan att behöva manuellt uppdatera informationen.

### JSON-formatet

För att utbyta data använder många webb-API:er JSON (JavaScript Object Notation), ett lätt och flexibelt format för datalagring och datautbyte. JSON-formatet består av nyckel-värdepar, vilket gör det enkelt att läsa och skriva för människor och att tolka och generera för maskiner.

* För att underlätta läsningen av JSON-data i webbläsaren kan du installera tillägget [JSON formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa).

## Ett publikt API

Som ett exempel på användning av publika API:er kommer vi att använda [api.chucknorris.io](https://api.chucknorris.io), en tjänst som erbjuder slumpmässiga skämt om Chuck Norris.

### Koden för att hämta data

För att hämta data från ett API i PHP kan vi använda funktionen `file_get_contents()` som läser innehållet från en URL som en sträng.

{% tabs %}
{% tab title="chucknorris.php" %}
```php
<?php
$url = "https://api.chucknorris.io/jokes/random"; // URL till API:t

$json = file_get_contents($url); // Anropa API:t och hämta JSON-data

// JSON-data är nu i $json som en sträng
?>
```
{% endtab %}
{% endtabs %}

Efter att ha hämtat JSON-datan, kan vi använda `json_decode()` för att omvandla JSON-strängen till ett PHP-objekt för enkel åtkomst till dess data.

{% tabs %}
{% tab title="chucknorris.php" %}
```php
$jsonData = json_decode($json); // Omvandla JSON-strängen till ett PHP-objekt
```
{% endtab %}
{% endtabs %}

### JSON-formatet exemplifierat

I vårt exempel innehåller JSON-datan information om ett skämt, inklusive dess text (value) och en bild (icon_url). Här är ett exempel på hur datan kan se ut:

```json
{
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "value": "For the next debate, Chuck Norris has volunteered to stuff all of the remaining GOP Presidential candidates into the tiny circus clown car they use for their arrival to the debate venue."
}
```

För att visa skämtet och bilden använder vi nycklarna `value` och `icon_url` från det PHP-objekt vi skapat med `json_decode()`.

{% tabs %}
{% tab title="chucknorris.php" %}
```php
$skamtet = $jsonData->value; // Plocka ut skämtet
$bilden = $jsonData->icon_url; // Plocka ut bilden

// Skriv ut skämtet och bilden
echo "<blockquote><img src=\"$bilden\" alt=\"Chuck Norris\">
$skamtet<footer>Chuck Norris</footer></blockquote>";
```
{% endtab %}
{% endtabs %}

## Sammanfattning av koden

Hela koden för att hämta och visa ett slumpmässigt Chuck Norris-skämt ser ut som följer:

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chuck Norris skämt</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Chuck Norris skämt</h1

>
        <?php
        $url = "https://api.chucknorris.io/jokes/random";

        $json = file_get_contents($url);
        $jsonData = json_decode($json);

        $skamtet = $jsonData->value;
        $bilden = $jsonData->icon_url;

        echo "<blockquote><img src=\"$bilden\" alt=\"Chuck Norris\">
        $skamtet<footer>Chuck Norris</footer></blockquote>";
        ?>
    </div>
</body>
</html>
```

## Tutorial

För en mer omfattande guide om hur du arbetar med JSON och API:er i PHP, rekommenderas följande tutorial: [PHP JSON complete tutorial](https://alexwebdevelop.com/php-json-backend/). Denna guide går igenom grunderna i JSON och hur man hanterar JSON-data i PHP för att skapa dynamiska webbapplikationer.