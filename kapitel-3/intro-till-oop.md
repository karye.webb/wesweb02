# Intro till OOP

Objektorienterad programmering (OOP) är en programmeringsparadigm som använder "objekt" – datastrukturer bestående av fält (attribut eller egenskaper) och metoder (funktioner) – för att designa applikationer och datorprogram. OOP fokuserar på att använda objekt som representerar abstraktioner av verkliga entiteter. Detta tillvägagångssätt underlättar större flexibilitet och återanvändning av kod.

## Youtube tutorial

För att lära dig grunderna i OOP med PHP rekommenderas en Youtube-serie som ger en omfattande introduktion till ämnet. Denna serie förklarar de grundläggande koncepten i OOP, inklusive klasser, objekt, arv, polymorfism, inkapsling och mycket mer.

* Se tutorialserien här: [Objektorienterad PHP för nybörjare](https://www.youtube.com/watch?v=LuWxwLk8StM&list=PL4cUxeGkcC9hNpT-yVAYxNWOmxjxL51Hy)

Detta är en utmärkt resurs för både nybörjare och mer erfarna utvecklare som vill fördjupa sin förståelse för OOP inom PHP. Videorna går igenom allt från grunderna till mer avancerade ämnen, vilket ger en solid grund att bygga vidare på.

* Tillhörande material och kodexempel från serien finns tillgängliga på GitHub: [iamshaunjp/object-oriented-php](https://github.com/iamshaunjp/object-oriented-php)

## Grundläggande begrepp i OOP

### Klasser och Objekt
En **klass** är en mall eller blueprint för att skapa objekt. Den definierar vilka egenskaper och metoder objekten som skapas från klassen kommer att ha. Ett **objekt** är en instans av en klass. När en klass instantieras (dvs. när ett objekt skapas från en klass), tilldelas objektet de egenskaper och metoder som definierats i klassen.

### Egenskaper (Properties)
**Egenskaper** är de variabler som finns inom en klass. De används för att lagra information om objektet.

### Metoder (Methods)
**Metoder** är de funktioner som definieras inom en klass. De används för att beskriva beteendet hos objekten som skapas från klassen.

### Konstruktor
En **konstruktor** är en speciell metod som automatiskt anropas när ett objekt skapas från en klass. Den används ofta för att initialisera objektets egenskaper.

### Arv (Inheritance)
**Arv** möjliggör för en klass att ärva egenskaper och metoder från en annan klass. Detta är användbart för att skapa en hierarki av klasser och återanvända kod.

### Inkapsling (Encapsulation)
**Inkapsling** är principen om att gömma interna detaljer i en klass och endast exponera det som är nödvändigt för objektets användning. Detta uppnås genom att använda åtkomstmodifierare som `public`, `private` och `protected`.

### Polymorfism
**Polymorfism** är konceptet att låta en metod ha olika implementationer baserade på kontexten den används i. Detta kan uppnås genom överlagring (overloading) och överskridning (overriding) av metoder.

---

Att lära sig OOP med PHP är ett viktigt steg för att utveckla mer strukturerade, modulära och återanvändbara applikationer. Genom att använda OOP-principer kan du skapa kod som är lättare att underh

ålla och utveckla över tid.