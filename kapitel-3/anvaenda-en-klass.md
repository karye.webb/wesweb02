# Validator klass 2:2

Att skapa en valideringsklass är ett vanligt och effektivt sätt att hantera validering av användarinput i webbapplikationer. Genom att använda objektorienterad programmering (OOP) kan vi skapa en återanvändbar och modulär valideringslogik.

## Stegen för att skapa en Validator-klass

### 1. Skapa en klass `Validator`

En klass fungerar som en mall för att skapa objekt. Vår `Validator`-klass kommer att innehålla metoder för att validera olika typer av input som användarnamn, e-postadress och lösenord.

### 2. Skapa en konstruktor

En konstruktor är en speciell metod som automatiskt körs när ett objekt skapas från klassen. Den kan användas för att initialisera objektets egenskaper.

### 3. Kontrollera att inga fält saknas

Innan validering börjar, är det bra att kontrollera att alla förväntade fält faktiskt finns i den data som ska valideras. Detta kan göras när data sätts i klassen.

### 4. Skapa funktioner för att validera

Vi skapar separata metoder för att validera olika typer av input:

- **4.1 Validera användarnamn**: Kontrollera att användarnamnet följer bestämda regler.
- **4.2 Validera e-post**: Använd PHP:s inbyggda filter för att validera e-postformat.
- **4.3 Validera lösenord**: Kontrollera att lösenordet uppfyller alla säkerhetskrav.

### 5. Returnera en array med alla fel

Efter att valideringarna genomförts, returneras en array som innehåller eventuella felmeddelanden associerade med varje fält.

## Implementering av `Validator`-klassen

### Skapa klassen `Validator`

```php
class Validator {
    private $errors = [];
    private $data;

    public function __construct() {
        $this->errors = [
            "username" => [],
            "password" => [],
            "email" => []
        ];
    }

    public function set($postdata) {
        $this->data = $postdata;
    }

    public function validateUsername() {
        if (!preg_match("/[a-zA-Z0-9]{6,12}/", $this->data["username"])) {
            $this->errors['username'][] = "Användarnamnet måste vara mellan 6 till 12 tecken och får endast innehålla bokstäver och siffror.";
        }
    }

    public function validatePassword() {
        // Lägg till valideringsregler som beskrivits ovan
    }

    public function validateEmail() {
        if (!filter_var($this->data["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'][] = "Ogiltigt e-postformat.";
        }
    }

    public function showErrors($type) {
        if (array_key_exists($type, $this->errors)) {
            foreach ($this->errors[$type] as $error) {
                echo "<p>$error</p>";
            }
        }
    }
}
```

### Använda klassen `Validator`

När klassen `Validator` har implementerats, kan vi använda den för att validera användarinput från ett formulär. Här är ett exempel på hur klassen kan användas:

```php
require_once "Validator.php";

$validator = new Validator();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $validator->set($_POST);
    $validator->validateUsername();
    $validator->validateEmail();
    $validator->validatePassword();
    // Ytterligare valideringar kan läggas till här
}
```

Och sedan för att visa felmeddelanden:

```php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $validator->showErrors("username");
    $validator->

showErrors("email");
    $validator->showErrors("password");
}
```

Genom att använda en `Validator`-klass kan vi effektivt centralisera och hantera valideringslogiken för användarinput i våra webbapplikationer, vilket leder till renare kod och bättre återanvändbarhet.