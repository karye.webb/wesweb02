# Intro till OOP och inloggningssystem

Objektorienterad programmering (OOP) är en programmeringsparadigm som använder objekt och klasser för att strukturera programvaran på ett modulärt och återanvändbart sätt. Ett inloggningssystem är ett vanligt scenario där OOP kan tillämpas för att hantera användarautentisering effektivt.

## Ett vanligt loginsystem för användare

### Tabellen för användaruppgifter

För att lagra användaruppgifter skapar vi en databastabell `admin` med kolumner för användarnamn (`anamn`) och en hash av användarens lösenord (`hash`).

```sql
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anamn` varchar(50) NOT NULL,
  `hash` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
```

### Registrera användare (utan OOP)

För att registrera en ny användare fångar vi upp formulärdata och sparar den i databasen efter att ha hashat lösenordet med `password_hash()`.

```php
<?php
// Inloggningsmodul med databas
include_once $_SERVER["DOCUMENT_ROOT"] . "/_databaser/konfig-db.php";

// Ta emot och hantera formulärdata
$anamn = filter_input(INPUT_POST, 'anamn', FILTER_SANITIZE_STRING);
$losen = filter_input(INPUT_POST, 'losen', FILTER_SANITIZE_STRING);

if ($anamn && $losen) {
    $hash = password_hash($losen, PASSWORD_DEFAULT);
    $sql = "INSERT INTO admin (anamn, hash) VALUES (?, ?);";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ss", $anamn, $hash);
    $stmt->execute();

    if ($stmt->affected_rows > 0) {
        echo "<p class=\"alert alert-success\">Användaren är registrerad</p>";
    } else {
        echo "<p class=\"alert alert-warning\">Kunde inte registrera användaren</p>";
    }

    $stmt->close();
    $conn->close();
}
?>
<!-- HTML för registreringsformulär -->
```

### Logga in användare (utan OOP)

För att logga in en användare matchar vi användarnamnet och verifierar det inskickade lösenordet mot hashen lagrad i databasen med `password_verify()`.

```php
<?php
// Inloggningsmodul med databas
include_once $_SERVER["DOCUMENT_ROOT"] . "/_databaser/konfig-db.php";

// Ta emot och hantera inloggningsdata
$anamn = filter_input(INPUT_POST, 'anamn', FILTER_SANITIZE_STRING);
$losen = filter_input(INPUT_POST, 'losen', FILTER_SANITIZE_STRING);

if ($anamn && $losen) {
    $sql = "SELECT hash FROM admin WHERE anamn = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $anamn);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        $stmt->bind_result($hash);
        $stmt->fetch();

        if (password_verify($losen, $hash)) {
            echo "<p class=\"alert alert-success\">Inloggningen lyckades</p>";
        } else {
            echo "<p class=\"alert alert-warning\">Felaktigt lösenord</p>";
        }
    } else {
        echo "<p class=\"alert alert-warning\">Användarnamnet finns inte</p>";
    }

    $stmt->close();
    $conn->close();
}
?>
<!-- HTML för inloggningsformulär -->
```

## OOP-varianten

Att omvandla ovanstående funktionalitet till OOP innebär att skapa en `Login`-klass som hanterar registrering och inloggning av användare på ett mer strukturerat sätt.

### Skapa en klass `Login`

Vi skapar en

 klass `Login` med metoder för att registrera nya användare (`registrera`) och för att kontrollera om en användare finns vid inloggning (`kontroll`).

```php
class Login {
    private $conn;

    public function __construct($conn) {
        $this->conn = $conn;
    }

    public function registrera($anamn, $losen) {
        // Implementering av användarregistrering
    }
    
    public function kontroll($anamn, $losen) {
        // Implementering av användarkontroll vid inloggning
    }
}
```

### Implementera metoder

```php
// Implementering av registrera()
public function registrera($anamn, $losen) {
    $hash = password_hash($losen, PASSWORD_DEFAULT);
    $sql = "INSERT INTO admin (anamn, hash) VALUES (?, ?);";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("ss", $anamn, $hash);
    $stmt->execute();

    if ($stmt->affected_rows > 0) {
        return 1;
    } else {
        return 0;
    }
}

// Implementering av kontroll()
public function kontroll($anamn, $losen) {
    $sql = "SELECT hash FROM admin WHERE anamn = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $anamn);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        $stmt->bind_result($hash);
        $stmt->fetch();
        if (password_verify($losen, $hash)) {
            return 3;
        } else {
            return 2;
        }
    } else {
        return 1;
    }
}
```

### Använda klassen `Login`

```php
<?php
require_once("./klasser/Login.php");
$login = new Login($conn);

// Exempel på användning för registrering
if ($anamn && $lösen) {
    $resultat = $login->registrera($anamn, $lösen);
    // Hantera resultat...
}

// Exempel på användning för inloggning
if ($anamn && $lösen) {
    $resultat = $login->kontroll($anamn, $lösen);
    // Hantera resultat...
}
?>
```

Genom att använda OOP för att hantera inloggningssystemet skapas en mer modulär och återanvändbar kodstruktur. Klassen `Login` kan lätt utökas med ytterligare funktionalitet, och genom att separera logiken från presentationen blir koden lättare att underhålla och utveckla.